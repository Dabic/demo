FROM openjdk:11-jdk-slim
ARG JAR_FILE=./build/libs/*.jar
COPY $JAR_FILE /demo/demo.jar
WORKDIR demo
ENTRYPOINT java -jar demo.jar